import * as React from 'react';
import { MyTableComponent } from './components';

export const App: React.StatelessComponent<{}> = () => {
  return (
    <div className="container-fluid">
      <MyTableComponent />
    </div>
  );
}
