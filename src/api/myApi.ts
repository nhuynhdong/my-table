import axios from "axios";

export const getAllInvestments = ({pageSize, pageNumber}) => {
   return axios
    .post("http://localhost:4000/graphql", {
      query:
      `query InvestParam($pageSize: Int!, $pageNumber: Int){
        getAllInvestments(pageSize: $pageSize, pageNumber: $pageNumber) {
          id
          investment
          commitmentDate
          marketValue
        }
      }`,
      variables: {pageSize, pageNumber}
    });
};
