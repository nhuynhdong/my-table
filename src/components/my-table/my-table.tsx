import * as React from "react";
import { InvestmentEntity } from "../../model/investment";
import { getAllInvestments } from "../../api/myApi";

const useInvestmentCollection = () => {
  const [investmentCollection, setInvestmentCollection] = React.useState<
    InvestmentEntity[]
  >([]);

  const loadInvestmentCollection = () => {
    getAllInvestments({pageSize: 10000, pageNumber: 0}).then(investmentCollection =>
      setInvestmentCollection(investmentCollection.data.data.getAllInvestments)
    );
  };
  return { investmentCollection, loadInvestmentCollection };
};

export const MyTableComponent = () => {
  const { investmentCollection, loadInvestmentCollection } = useInvestmentCollection();

  React.useEffect(() => {
    loadInvestmentCollection();
  }, []);

  return (
    <>
      <table className="my-table">
        <thead>
          <tr>
            <th>Investment</th>
            <th>Commitment Date</th>
            <th>Market Value</th>
          </tr>
        </thead>
        <tbody>
          {investmentCollection.map(investment => (
            <InvestmentRow key={investment.id} invest={investment} />
          ))}
        </tbody>
      </table>
    </>
  );
};

const InvestmentRow = ({ invest }: { invest: InvestmentEntity }) => (
  <tr>
    <td>
      <span>{invest.investment}</span>
    </td>
    <td>
      <span>{invest.commitmentDate}</span>
    </td>
    <td>
      <span>{invest.marketValue}</span>
    </td>
  </tr>
);
