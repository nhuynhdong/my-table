export interface InvestmentEntity {
  id: number;
  investment: string;
  commitmentDate: string;
  marketValue: number;
}
