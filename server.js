const express = require('express');
const yargs = require('yargs');
const fs = require('fs');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const cors = require('cors');
const schema = buildSchema(fs.readFileSync('./server/schema.gql', 'utf8'));
const argv = yargs.argv;
const app = express();

const root = {
  getAllInvestments: ({ pageSize = 10000, pageNumber = 0 }) => {
    var investmentData = [];
    for (var i = 0; i < 10000; i++) {
      investmentData.push({
        id: i,
        investment: "Investment " + i,
        commitmentDate: new Date(
          2019,
          (Math.random() * 12 | 1) + 1,
          (Math.random() * 28 | 1) + 1
        ).toLocaleDateString(),
        marketValue: Math.floor(Math.random() * 100000 + 1)
      });
    }

    return investmentData.slice(pageNumber * pageSize, pageNumber * pageSize + pageSize);;
  }

};
app.use(cors());
app.use(
  '/graphql',
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);

const port = argv.port || 4000;
app.listen(port, () => console.log(`App listening on port ${port}!`));
